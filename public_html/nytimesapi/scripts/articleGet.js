//Hide previous results while search commences for new query
$(".list-group-item").hide();

$("#artButton").click(function () {

var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";

var queryBuild = $("#formArtSearch").serializeArray();

//sanity check the JSON objects being used for the URL build
console.log(queryBuild);

//Add the variable storing the URL string to the API key (this should get handled via a PHP file if for a genuine service) and form parameters, thats passed to NY times API
url += '?' + "api-key=1e6ce3e2859e45b28f260cd47694f231&" + $.param(queryBuild);

//Log the final URL
console.log(url);

// send that URL to NY times API and log the responses by looping in the available divs
$.ajax({
  url: url,
  method: 'GET',
}).done(function(result) {
 $(".list-group-item-heading, .img-wrapper, .span-wrapper").empty();

  console.log(result);

   $('.list-group-item-heading').each(function (i, v) {
       try{
        var results = "<a href=" + result.response.docs[i].web_url + ">" + result.response.docs[i].headline.main + "</a>";
        $(v).append(results)
      } catch (err) {
        var results = "Lack of results - search again";
        $(v).append(results);
      }
    }
      );

      $('.span-wrapper').each(function (i, v) {
                var results = "<span>" + result.response.docs[i].snippet + "</span>";
                 $(v).append(results);
                 }
                 );

      $('.img-wrapper').each(function (i, v) {
            try {
                    var results = "<img class='image-cover' src=" + "https://www.nytimes.com/" + result.response.docs[i].multimedia[0].url + ">";
                   $(v).append(results);

               } catch (err) {
                 var results = "<img class='image-cover' src=" + "img/" + "workshop.jpg" + ">";
                $(v).append(results);
             }
}
          );







    $(".list-group-item").show();

}).fail(function(err) {
  throw err;
});

});
